/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package name;

import java.awt.HeadlessException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.SimpleTimeZone;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
/**
 *
 * @author 20shustind
 */
public class Name {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String DOB = "";
        String name = "";
        int month;
        int day;
        int year;
        try {
            name = JOptionPane.showInputDialog("What is your name?");
            DOB = JOptionPane.showInputDialog("What is your Date of Birth? (MONTH/DAY/YEAR)");
        } catch (HeadlessException e) {
            System.err.println("An error occured.");
            return;
        }
        Pattern reg = Pattern.compile("(\\d+)\\/(\\d+)\\/(\\d+)");
        Matcher m = reg.matcher(DOB);
        if (m.matches()) {
            month = Integer.parseInt(m.group(1));
            day = Integer.parseInt(m.group(2));
            year = Integer.parseInt(m.group(3));
            if (month > 12 || month < 0 || day > 31 || day < 0 || year < 0) {//KEEP THE CAVEMEN OUT!
		    System.err.println("Invalid Date.");
		    return;
            }
            String[] ids = TimeZone.getAvailableIDs(-4 * 60 * 60 * 1000);
            if (ids.length == 0) {
                System.err.println("An error occured.");
                System.exit(0);
            }
            SimpleTimeZone edt = new SimpleTimeZone(-8 * 60 * 60 * 1000, ids[0]);
            edt.setStartRule(Calendar.APRIL, 1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);
            edt.setEndRule(Calendar.OCTOBER, -1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);
            Calendar cal = new GregorianCalendar(edt);
            Calendar now = new GregorianCalendar(edt);
            cal.set(year, month-1, day);
            now.setTime(new Date());
            int dayAge = (int) TimeUnit.MILLISECONDS.toDays(Math.abs(now.getTimeInMillis() - cal.getTimeInMillis()));
            int years = (int)Math.floor(dayAge/365.0);
            int days = dayAge % 365;
            System.out.println("Hello, " + name + "! You are " + years+ " years old and " + days + " days old.");
            switch (name.charAt(0)) {
                case 'a':
                case 'e':
                case 'i':
                case 'o':
                case 'u':
                    System.out.println("Your name starts with a vowel!");
                    break;
                default:
                    System.out.println("Your name does not start with a vowel!");
                    break;
            }
            int vowels = 0;
            for (int i = 0; i < name.length(); i++) {
                switch (name.charAt(i)) {
                    case 'a':
                    case 'e':
                    case 'i':
                    case 'o':
                    case 'u':
                        vowels++;
                        break;
                }
           }
           System.out.println(vowels == 0 ? "You have no vowels in your name!" : "You have " + vowels + " vowel" + (vowels > 1 ? "s" : "") + " in your name!");
        }
	else
            System.err.println("No match was found. Please enter valid input.");
        return;
    }
    
}
